package com.example.demo.repository;

import com.example.demo.model.Doggy;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DoggyRepository extends JpaRepository<Doggy,Integer> {
}
